# Dictionary/Flash Card app
An (in progress) app that allows users to search for words in the Japanese-English dictionary JMDict and review them using flashcards employing spaced repetition, a technique that increases the interval between reviews with each subsequent correct answer. Currently the app has an export feature along with a dynamic dark/light theme. It can technically be used to study as-is but it hasn't been tested for scale yet.

###Plans to add:
OCR(Optical Character Recognition) lookup, import feature, custom theme support, sentence/conjugation support, and various UI improvements

## Getting Started

* [Download the installer](https://nodejs.org/) for Node.js 6 or greater.
* Install [Android Studio](https://developer.android.com/studio/index.html) and [Java JDK 8](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
* Install the ionic CLI globally: `npm install -g ionic`
* Clone this repository: `git clone git@bitbucket.org:zekeh/dictionary-app.git`.
* Run `npm install` from the project root.
* Create JMdict.sqlite with [JMdict-to-sqlite3](https://github.com/Top-Ranger/jmdict-to-sqlite3)
* Run `ionic cordova build android` to create an APK.(`ionic cordova build ios` may or may not work as it has not been tested for ios yet, but feel free to try and let me know :) )
## Built With

* [Ionic](https://ionicframework.com/) - The framework used

## Acknowledgments

* [JMDict](http://www.edrdg.org/jmdict/edict.html), a multilingual Japanese dictionary which contains English, French and German translations for around 176,000 Japanese entries.

* [Houhou-SRS](http://houhou-srs.com/), a windows-only app that is similar to mine and inspired me to make my own personalized study app

* [JMdict-to-sqlite3](https://github.com/Top-Ranger/jmdict-to-sqlite3), the script I used to make the database

###Preview images:

![Home](sampleimg/home.png)

![Review](sampleimg/review.png)

![Dictionary](sampleimg/dictionary.png)

![Settings](sampleimg/settings.png)
