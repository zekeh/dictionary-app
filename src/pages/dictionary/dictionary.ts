import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { HomePage } from '../home/home';
//import Romaji from 'romaji';
import Kuroshiro from "kuroshiro";
import { Platform } from 'ionic-angular';
// import Converter from "jp-conversion";
import { DictProvider } from '../../providers/dict/dict';
import { NativeStorage } from '@ionic-native/native-storage';
//import deconstructJapanese from 'deconstruct-japanese';
// import { Keyboard } from '@ionic-native/keyboard';
import { DatabaseProvider } from '../../providers/database/database';

@Component({
  selector: 'page-dictionary',
  templateUrl: 'dictionary.html'
})
export class DictionaryPage {
  public dict: any;
  public words = [];
  public results: any[] = [];
  public isInstantiated = false;
  public vocab = "";
  constructor(public navCtrl: NavController,
    public httpClient: HttpClient,
    public dictionaryProvider: DictProvider,
    public ns: NativeStorage,
    public platform: Platform,
    public dbProvider: DatabaseProvider) {
  }
  doInfinite(infiniteScroll) {
    setTimeout(() => {
      infiniteScroll.complete();
      if (this.results.length == 1000) {
        infiniteScroll.disabled = true;
      }
      for (let i = 0; i < 58; i++) {
        if (this.words && this.words.length != undefined) {
          this.words.push(this.results[this.words.length]);
        }
      }
    }, 500);
  }
  add(i: number) {
    //fnd a way to only add the reading the user clicked on
    let definition = "";
    for (let s of this.results[i].gloss) {
      definition += (s + "; ");
    }
    //remove last ; and add spaces
    definition = definition.substring(0, definition.length - 2);
    definition = definition.toString().replace(/,/gi, ", ");
    this.navCtrl.push(HomePage, {
      word: (this.results[i].kanji[0] == ""? this.results[i].reading[0] : this.results[i].kanji[0]),
      reading: this.results[i].reading[0],
      meaning: definition,
      show: true
    },
    {animate: false});
  }
  
  search(val: string) {
    //alert("you searched!")
    this.results = [];
    if (val.length <= 1 && !Kuroshiro.Util.hasJapanese(val)) { return; }
    this.dictionaryProvider.search(val).then(data => {
      this.results = data;
      //alert(this.results.length.toString() + ' found')
    })
      .catch(err => {
        //alert(err);
        console.log(err)
      })
      // if(this.results.length > 0){
      //   this.keyboard.hide();
      // }
    // console.log("Results: ", this.results)
  }
  otherForm(i: number, r: any) {
    console.log("you clicked on other form", i)
    let temp = r[0]
    r[0] = r[i + 1];
    r[i + 1] = temp;
    console.log(temp);
  }
}
