import { Component } from '@angular/core';
import { DatabaseProvider } from '../../providers/database/database';
import { NavParams, Platform, NavController } from 'ionic-angular';
import { DictProvider } from '../../providers/dict/dict';
import { NativeStorage } from '@ionic-native/native-storage';
//import { Observable } from "rxjs/Observable";
import 'rxjs/add/observable/interval';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  public word: string = "";
  public reading: string = "";
  public meaning: string = "";
  public sentence: string = "";
  public show = false;
  public reviewCount: number = 0;
  public vocabs: any[];
  constructor(public navCtrl: NavController,
    private dbProvider: DatabaseProvider,
    private navParams: NavParams,
    private platform: Platform,
    public dictionary: DictProvider,
    private ns: NativeStorage) {
    this.word = navParams.get('word')
    this.reading = navParams.get('reading')
    this.meaning = navParams.get('meaning')
    this.show = navParams.get('show')
    this.platform.ready().then((readySource) => {
      this.ns.getItem('dead').then(data => {
        this.dbProvider.deleteAfterXDays(data)
      })
    })
  }
  ionViewDidLeave() {
    this.show = false;
  }
  ionViewDidEnter() {
    this.changeDisplay();
  }
  ionViewDidLoad() {
    this.changeDisplay();
  }
  addVocab() {
    if (!this.show) {
      this.show = true;
    }
    else {
      this.dbProvider.addVocab(this.word, this.reading, this.sentence, this.meaning);
      this.word = "";
      this.meaning = "";
      this.reading = "";
      this.sentence = ""; //clear form
      this.show = false;
      this.changeDisplay();
    }
  }
  getAmountOfReviews() {
    this.dbProvider.getVocab().then(data => {
      this.reviewCount = data.length;
    })
  }
  showAll() {
    this.dbProvider.getAllVocab().then(data => {
      return data;
    })
  }
  async import() {
    //find a way to let user format it and stuff
    try {
      this.dbProvider.import();
      this.changeDisplay();
    } catch (err) {
      alert(err);
    }
  }
  changeDisplay() {
    this.getAmountOfReviews();
  }

}
