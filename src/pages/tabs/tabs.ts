import { Component } from '@angular/core';
import { SettingsPage } from '../settings/settings'
import { HomePage } from '../home/home';
import { DictionaryPage } from '../dictionary/dictionary';
import { ReviewPage } from '../review/review';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = ReviewPage;
  tab2Root = HomePage;
  tab3Root = DictionaryPage;
  tab4Root = SettingsPage;

  constructor() {

  }
}
