import { Component } from '@angular/core';
import { IonicPage, NavController, Events } from 'ionic-angular';
import { DatabaseProvider } from '../../providers/database/database';
import { NativeStorage } from '@ionic-native/native-storage';

/**
 * Generated class for the SettingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {
  public day: number;
  public light: boolean = true;
  constructor(public navCtrl: NavController,
    public event: Events,
    private dbProvider: DatabaseProvider,
    public ns: NativeStorage) {
    this.ns.getItem('theme').then(data => {
      if (data == 'theme-noir') {
        this.light = false;
      }
    })
    this.ns.getItem('dead').then(data =>{
      this.day = data;
    })
    console.log(this.day)
  }
  click() {
    this.event.publish('theme:toggle');
    this.light = !this.light;
    this.ns.setItem('theme', this.light ? 'theme-light' : 'theme-noir');
  }
  deleteAfterXDays(days: number) {
    console.log(days);
    if (days) {
      this.ns.setItem('dead', days);
      this.dbProvider.deleteAfterXDays(days);
      alert("Set Successfully");
    }
  }
  export() {
    this.dbProvider.saveAsCsv();
  }
}
