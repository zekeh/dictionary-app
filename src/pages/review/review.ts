import { Component } from '@angular/core';
import { DatabaseProvider } from '../../providers/database/database';
import { FlipProvider } from '../../providers/flip/flip';

@Component({
  selector: 'page-review',
  templateUrl: 'review.html'
})
export class ReviewPage {

  shown: boolean = false;
  vocabs = [];
  wrongBit = false;
  currentCard = 0;
  // TODO: make it centered even if no sentence
  constructor(private dbProvider: DatabaseProvider,
    private flipper: FlipProvider) {
  }
  ionViewDidEnter() {
    try {
      this.getVocab();
    }
    catch (err) {
    }
  }
  ionViewDidLeave() {
    this.shown = false;
  }
  getVocab() {
    //this.vocabs = [1,2,3,4]
    this.dbProvider.getVocab().then(data => {
      this.vocabs = data;
    })
    for (let i = 0; i < this.vocabs.length; i++) {
      this.vocabs[i].wrongBit = false;
      // if (!Kuroshiro.Util.hasKanji(this.vocabs[i].vocab.toString())) {
      //   this.vocabs[i].reading = "test";
      // }
    }
    this.vocabs = this.shuffle(this.vocabs); //so it isnt reviewed in same order every time
  }
  showButtons() {
    this.shown = true;
  }
  shuffle(a) {
    //https://stackoverflow.com/a/6274381
    let j, x, i;
    for (i = a.length - 1; i > 0; i--) {
      j = Math.floor(Math.random() * (i + 1));
      x = a[i];
      a[i] = a[j];
      a[j] = x;
    }
    return a;
  }
  wrongAnswer(word: string) {
    this.vocabs[this.currentCard].wrongBit = true;
    this.shuffle(this.vocabs)
    this.flipper.flipped = false;
    this.shown = false;
  }
  rightAnswer(word: string) {
    let grade = this.vocabs[this.currentCard].grade;
    let delay = this.vocabs[this.currentCard].delay;
    try {
      if (this.vocabs[this.currentCard].wrongBit) {
        this.dbProvider.answered(1 / 2.5, word, delay, grade);
      } else {
        this.dbProvider.answered(2.5, word, delay, grade);
      }
    } catch{ }

    this.vocabs.splice(this.currentCard, 1);
    this.shown = false;
  }
}
