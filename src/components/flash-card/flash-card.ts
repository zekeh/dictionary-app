import { Component } from '@angular/core';
import { FlipProvider } from '../../providers/flip/flip';

//https://www.joshmorony.com/build-a-custom-flash-card-component-in-ionic-2/ -change up later
/**
 * Generated class for the FlashCardComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
 //https://www.joshmorony.com/creating-custom-form-input-components-with-ionic-angular/
@Component({
  selector: 'flash-card',
  templateUrl: 'flash-card.html'
})
export class FlashCardComponent {
  constructor(private flipper: FlipProvider) {
    this.flipper.flipped = false;
  }
  flip() {
    if (!this.flipper.flipped) {
      this.flipper.flipped = !this.flipper.flipped;
    }
  }
}
