import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Events } from 'ionic-angular';
import { TabsPage } from '../pages/tabs/tabs';
import { NativeStorage } from '@ionic-native/native-storage';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any = TabsPage;
  theme: String = 'theme-light';

  constructor(public platform: Platform,
    public event: Events,
    public ns: NativeStorage) {
    //https://medium.freecodecamp.org/how-to-dynamically-theme-your-ionic-application-and-make-your-users-happy-ffa17e15dbf7
    platform.ready().then(() => {
      if (this.ns.getItem('theme')) {
        this.ns.getItem('theme').then(data => {
          this.theme = data;
        })
      }
      event.subscribe('theme:toggle', () => {
        this.toggleTheme();
      });
    });
  }
  toggleTheme() {
    if (this.theme == 'theme-light') {
      this.theme = 'theme-noir';
    } else {
      this.theme = 'theme-light';
    }
  }
}
