import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { DictionaryPage } from '../pages/dictionary/dictionary';
import { ReviewPage } from '../pages/review/review';
import { TabsPage } from '../pages/tabs/tabs';
import { SettingsPage } from '../pages/settings/settings'
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { FlashCardComponent } from '../components/flash-card/flash-card';
import { SQLite } from '@ionic-native/sqlite';
import { HttpClientModule } from '@angular/common/http';
import { DatabaseProvider } from '../providers/database/database';
import { NgProgress } from '@ngx-progressbar/core';
import { FileChooser } from '@ionic-native/file-chooser';
import { File } from '@ionic-native/file';
import { NativeStorage } from '@ionic-native/native-storage';
import { DictProvider } from '../providers/dict/dict';
import { FilePath } from '@ionic-native/file-path';
import { FlipProvider } from '../providers/flip/flip';
import { HttpClient } from '@angular/common/http';
import { SQLitePorter } from '@ionic-native/sqlite-porter';
import { SqliteDbCopy } from '@ionic-native/sqlite-db-copy';
import { Keyboard } from '@ionic-native/keyboard';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    DictionaryPage,
    ReviewPage,
    TabsPage,
    FlashCardComponent,
    SettingsPage,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    DictionaryPage,
    ReviewPage,
    TabsPage,
    SettingsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    SQLite,
    DatabaseProvider,
    FileChooser,
    NativeStorage,
    NgProgress,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    DictProvider,
    File,
    FilePath,
    FlipProvider,
    HttpClient,
    SQLitePorter,
    SqliteDbCopy,
    Keyboard
  ]
})
export class AppModule {}
