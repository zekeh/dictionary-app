import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import Romaji from 'romaji';
import { Events } from 'ionic-angular';
import 'rxjs/add/operator/catch';
import { DatabaseProvider } from '../database/database';



/*
  Generated class for the DictProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DictProvider {
  constructor(
    public httpClient: HttpClient,
    public events: Events,
    public dbProvider: DatabaseProvider) {

  }
  search(val: string) {
    //val = "真"
    //check if romaji and convert
    let romaji = Romaji.toHiragana(val);
    let data = [romaji, val, val, val.toLowerCase()];
    try {
      return this.dbProvider.db.executeSql(`SELECT * FROM entry WHERE instr(reading, (?)) or instr(kanji, (?)) or instr(gloss, (?)) or instr(gloss, (?)) order by length(reading);`, data).then(data => {
        //alert(JSON.stringify(data))
        return this.getEntries(data);
      }, err => {
        //alert(JSON.stringify(err))
        console.log(err)
        return []
      });
    } catch (err) {
      //alert(JSON.stringify(err))
      console.log(err)
    }
  }
  formatResults(results: any) {
    //turn strings of multiple kanji/reading/definition into array
    for(let i = 0; i < results.length; i++){
      results[i].gloss = results[i].gloss.split("(,)")
      results[i].reading = results[i].reading.split("(,)")
      results[i].kanji = results[i].kanji.split("(,)")
      results[i].position = results[i].position.split("(,)")
    }
    return results
  }
  getEntries(data: any) {
    let entries = [];
    if (data.rows.length > 0) {
      for (let i = 0; i < data.rows.length; i++) {
        entries.push({
          kanji: data.rows.item(i).kanji,
          reading: data.rows.item(i).reading,
          gloss: data.rows.item(i).gloss,
          position: data.rows.item(i).position
        });
      }
    }
    //alert(entries)
    return this.formatResults(entries);
  }
}
