import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { FileChooser } from '@ionic-native/file-chooser';
import { File } from '@ionic-native/file';
import { FilePath } from '@ionic-native/file-path';
import { Platform } from 'ionic-angular';
import { SqliteDbCopy } from '@ionic-native/sqlite-db-copy';
import { NativeStorage } from '@ionic-native/native-storage';

/*
  Generated class for the DatabaseProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DatabaseProvider {
  public db: SQLiteObject;
  public isInstantiated = false;
  constructor(public http: HttpClient,
    private sqlite: SQLite,
    private fileChooser: FileChooser,
    private file: File,
    private filePath: FilePath,
    public platform: Platform,
    private sqliteDbCopy: SqliteDbCopy,
    private ns: NativeStorage) {
    // console.log('Hello DatabaseProvider Provider');
    this.platform.ready().then(() => {
      if (!this.isInstantiated)
        this.initializeDatabase();
    })
  }
  delete(val: string) {
    let data = [val];
    try {
      return this.db.executeSql(`DELETE FROM Srs
WHERE vocab = ?;`, data).then(data => {
          return data;
        }, err => {
          console.log('Error deleting vocab');
          return err;
        });
    } catch (err) {
      console.log(err.toString())
    }
  }
  initializeDatabase() {
    this.ns.getItem('copied').then(() => {
      this.sqlite.create({
        name: 'jmdict.db',
        location: 'default',
        // @ts-ignore
        androidDatabaseProvider: 'system'
      })
        .then((db: SQLiteObject) => {
          this.db = db;
          this.isInstantiated = true;
        })
        .catch(e => console.log('Initialization error'));

    })
      .catch(err => {
        this.sqliteDbCopy.copy('jmdict.db', 0)
          .then((res: any) => {
            this.sqlite.create({
              name: 'jmdict.db',
              location: 'default',
              // @ts-ignore
              androidDatabaseProvider: 'system'
            })
              .then((db: SQLiteObject) => {
                this.db = db;
                this.isInstantiated = true;
                this.ns.setItem('copied', true)
              })
              .catch(e => console.log('Initialization error'));
          })
          .catch((error: any) => console.error(error));
      })
  }
  async deleteAfterXDays(days: number) {
    while(!this.isInstantiated){
      await this.delay(50)
    }
    let hours = days * 24;
    let data = [hours];
    try {
      return this.db.executeSql(`DELETE FROM Srs
WHERE delay >= ?;`, data).then(data => {
          return data;
        }, err => {
          console.log('Error deleting vocab');
          alert('Error deleting vocab')
          return err;
        });
    } catch (err) {
      console.log(err.toString())
    }
  }
  async saveAsCsv() {
    let csv: any = await this.convertToCSV()
    let fileName: any = "srs.csv"
    this.file.writeFile(this.file.externalRootDirectory, fileName, csv, { replace: true })
      .then(
        _ => {
          alert("srs.csv Saved Successfully")
        })
  }

  delay(timer) {
    return new Promise(resolve => {
      timer = timer || 2000;
      setTimeout(function() {
        resolve();
      }, timer);
    });
  }
  async convertToCSV() {
    let json: any;
    this.getAllVocab().then(data => {
      json = data;
    })
    while (!json) {
      await this.delay(50);
    }
    for (let i = 0; i < json.length; i++) {
      json[i].NextAnswerDate = this.unformatTime(json[i].NextAnswerDate.toString())
    }
    let fields = Object.keys(json[0])
    let replacer = function(key, value) { return value === null ? '' : value }
    let csv = json.map(function(row) {
      return fields.map(function(fieldName) {
        return JSON.stringify(row[fieldName], replacer)
      }).join(',')
    })
    csv.unshift(fields.join(',')) // add header column
    csv = csv.join('\r\n');
    console.log(csv)
    return csv;
  }
  formatTime(d: Date) {
    return d.getFullYear().toString() + (("0") + d.getMonth().toString()).slice(-2) +
      (("0") + d.getDate().toString()).slice(-2) + (("0") + d.getHours().toString()).slice(-2) +
      (("0") + d.getMinutes().toString()).slice(-2);
  }
  unformatTime(time: string) {
    //2019 06 31 21 23
    let date = new Date();
    date.setFullYear(parseInt(time.substring(0, 4)));
    date.setMonth(parseInt(time.substring(4, 6)));
    date.setDate(parseInt(time.substring(6, 8)));
    date.setHours(parseInt(time.substring(8, 10)));
    date.setMinutes(parseInt(time.substring(10, 12)));
    return date;
  }
  addVocab(word: string, reading: string, sentence: string = "", meaning: string) {
    if (word == "" || !word) {
      return;
    }
    let d: string | Date;
    d = new Date();
    d.toUTCString();
    d = this.formatTime(d);
    let data = [word, reading, sentence, meaning, d];
    try {
      return this.db.executeSql(`INSERT INTO Srs (vocab, reading, sentence, meanings, NextAnswerDate)
     VALUES (?, ?, ?, ?, ?)`, data).then(data => {
          return data;
        }, err => {
          console.log('Error adding vocab');
          alert(JSON.stringify(err))
          return err;
        });
    } catch (err) {
      console.log(err)
    }
  }
  getWords(data: any) {
    let words = [];
    if (data.rows.length > 0) {
      for (let i = 0; i < data.rows.length; i++) {
        words.push({
          vocab: data.rows.item(i).vocab,
          reading: data.rows.item(i).reading,
          sentence: data.rows.item(i).sentence,
          meanings: data.rows.item(i).meanings,
          grade: data.rows.item(i).grade,
          NextAnswerDate: data.rows.item(i).NextAnswerDate,
          delay: data.rows.item(i).delay
        });
      }
    }
    return words;
  }
  async getVocab() {
    while(!this.isInstantiated){
      await this.delay(50)
    }
    let d: string | Date;
    d = new Date();
    d.toUTCString();
    d = this.formatTime(d);
    let time = [d]
    try {
      return this.db.executeSql(
        `SELECT vocab, reading, sentence, meanings, grade, delay, NextAnswerDate FROM Srs
      WHERE NextAnswerDate <= ? `, time).then((data) => {
          return this.getWords(data);
        }, err => {
          console.log('error getting vocab');
          return [];
        });
    } catch (err) {
      console.log(err)
    }
  }
  async getAllVocab() {
    while(!this.isInstantiated){
      await this.delay(50)
    }
    try {
      return this.db.executeSql(
        `SELECT vocab, reading, sentence, meanings, grade, delay, NextAnswerDate FROM Srs
      `, []).then((data) => {
          return this.getWords(data);
        }, err => {
          console.log('error getting vocab');
          return [];
        });
    } catch (err) {
      console.log(err)
    }
  }
  answered(ease: number, word: string, delay: number, grade: number) {
    let gradeNum = grade;
    if (grade == 0 && ease < 1) { // D and got answer wrong
      gradeNum = 0;
    }
    else if (grade == 7 && ease > 1) { //A+ and got right answer
      gradeNum = 7;
    }
    else if (ease < 1) {
      gradeNum += -1;
    }
    else {
      gradeNum += 1;
    }
    let newDelay = 4;
    if (ease * delay > 4) {
      newDelay = ease * delay;
    }
    let answerDate: string | Date;
    answerDate = new Date();
    answerDate.setHours(answerDate.getHours() + newDelay)
    answerDate = this.formatTime(answerDate);
    let data = [answerDate, newDelay, gradeNum, word]
    this.db.executeSql(`UPDATE Srs SET NextAnswerDate = (?),
    delay = (?),
    grade = (?)
    WHERE vocab =(?);`, data).then(data => {
        return data;
      }, err => {
        console.log('Error updating vocab');
        return err;
      });
  }
  CSVToArray(strData, strDelimiter) {
    // ref: http://stackoverflow.com/a/1293163/2343
    strDelimiter = (strDelimiter || ",");
    // Create a regular expression to parse the CSV values.
    let objPattern = new RegExp(
      (
        // Delimiters.
        "(\\" + strDelimiter + "|\\r?\\n|\\r|^)" +
        // Quoted fields.
        "(?:\"([^\"]*(?:\"\"[^\"]*)*)\"|" +
        // Standard fields.
        "([^\"\\" + strDelimiter + "\\r\\n]*))"
      ),
      "gi"
    );
    let arrData = [[]];
    let arrMatches = null;
    while (arrMatches = objPattern.exec(strData)) {
      let strMatchedDelimiter = arrMatches[1];
      if (
        strMatchedDelimiter.length &&
        strMatchedDelimiter !== strDelimiter
      ) {
        arrData.push([]);
      }
      let strMatchedValue;
      if (arrMatches[2]) {
        strMatchedValue = arrMatches[2].replace(
          new RegExp("\"\"", "g"),
          "\""
        );
      } else {
        strMatchedValue = arrMatches[3];
      }
      arrData[arrData.length - 1].push(strMatchedValue);
    }
    return (arrData);
  }
  import() {
    //https://forum.ionicframework.com/t/using-filechooser-and-filepath-to-read-text-file/125645/2
    alert(`Import csv with format: 'vocab, reading, sentence, meanings, grade, next answer date, and delay(amount of hours between reviews)'`);
    this.fileChooser.open().then(file => {
      this.filePath.resolveNativePath(file).then(resolvedFilePath => {
        let path = resolvedFilePath.substring(0, resolvedFilePath.lastIndexOf('/'));
        let file = resolvedFilePath.substring(resolvedFilePath.lastIndexOf('/') + 1, resolvedFilePath.length);
        const b = this.readCsvData(path, file)
      }).catch(err => {
        //alert(JSON.stringify(err));
      });
    }).catch(err => {
      //alert(JSON.stringify(err));
    });
  }
  readCsvData(path, file) {
    this.file.readAsText(path, file)
      .then(content => {
        console.log("File-Content: " + JSON.stringify(content));
        let b = this.CSVToArray(content, ',')
        for (let i = 1; i < b.length; i++) {
          this.addVocabFromCSV(b[i])
        }
        alert("Import successful")
      })
      .catch(err => {
        console.log(err);
        //alert(JSON.stringify(err));
      });
  }
  addVocabFromCSV(data) {
    //format NextAnswerDate
    data[5] = this.formatTime(new Date(data[5].toString()))
    return this.db.executeSql(`INSERT INTO Srs (vocab, reading, sentence, meanings, grade, NextAnswerDate, delay)
     VALUES (?, ?, ?, ?, ?, ?, ?)`, data).then(data => {
        return data;
      }, err => {
        console.log('Error adding vocab');
        return err;
      });
  }
}
