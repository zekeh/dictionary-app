import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the FlipProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class FlipProvider {
  public flipped: boolean = false;
  constructor(public http: HttpClient) {
    console.log('Hello FlipProvider Provider');
  }
  flip(){
    this.flipped = !this.flipped;
  }

}
